# Josephmark Developer Task #

Your task is to develop a Pokedex using the Figma design that we provided. The Pokemon data should be fetched from the [PokeAPI](https://pokeapi.co/), and the number of times a Pokemon has been added to a party should be stored in a database of your choosing (eg. Firebase).

You can build the front end using any JS framework of your choosing.

A Figma file has been provided that includes designs for the two core pages - the index page and the party page. Some of the tasks will require you to make your own design decisions.

## Front End Tasks ##

| # | Task Details |
| - | ---- |
|  | __Index Page__ |
| 1 | Fetch the first generation Pokemon from the PokeAPI. |
| 2 | Display the Pokemon in a grid and paginate the results by 12. |
| 3 | Fetch the next 12 Pokemon when the user scrolls to the end of the grid. |
| 4 | When a Pokemon is clicked, add or remove them from the party. |
| 5 | Where it says "Added to X parties", display the total number of parties that Pokemon is currently in. (see back end tasks) |
| 6 | Display the number of Pokemon that have been loaded at the bottom of the grid. |
| 7 | Display the Pokemon that are currently in the party on the right hand side of the grid. |
|  | __Party Page__ |
| 1 | Display the Pokemon that are currently in the party in a three column grid. |
| 2 | The Pokemon can be given a nickname (only applies to the Party, not the index page) by clicking on the name text. |
| 3 | The Pokemon can be removed from the party by clicking the 'X' icon. |

__*__ The following restrictions apply to your party:

1. It can only hold up to six Pokemon at a time.
2. It cannot contain duplicates.

## Back End Tasks ##

| # | Task Details |
| - | ---- |
| 1 | Keep track of the total number of times each Pokemon has been added to a party. |

The number for the total number of times a Pokemon has been added to a party should be stored in your own database. This number represents the number of times each Pokemon has been added across all users and sessions.

### Notes ###
- Feel free to extend the task by adding your own features.
- Keep notes on any important decisions you make along the way.
